class TreeNode {
  constructor (value) {
    this.value = value
  }

  insert (branch) {
    if (branch.value > this.value) {
      if (this.right) {
        this.right.insert(branch)
      } else {
        this.right = branch
      }
    } else if (branch.value < this.value) {
      if (this.left) {
        this.left.insert(branch)
      } else {
        this.left = branch
      }
    } else {
      // ignore existing entry
    }
  }

  find (value) {
    if (value > this.value) {
      if (this.right) {
        return this.right.find(value)
      }
      return false
    } else if (value < this.value) {
      if (this.left) {
        return this.left.find(value)
      }
      return false
    }
    return true
  }
}

module.exports = TreeNode
