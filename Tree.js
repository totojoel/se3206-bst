const TreeNode = require('./TreeNode')

class Tree {
  insert (entry) {
    const newNode = new TreeNode(entry)

    if (this.root) {
      this.root.insert(newNode)
    } else {
      this.root = newNode
    }
  }

  find (value) {
    if (this.root) {
      return this.root.find(value)
    }
    return false
  }
}

module.exports = Tree
