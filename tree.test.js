const Tree = require('./Tree')

test('check if new node is inserted', () => {
  const kahoy = new Tree()

  expect(kahoy.find(3)).toBeFalsy()
  kahoy.insert(3)
  expect(kahoy.find(3)).toBeTruthy()
})

test('check if value exists', () => {
  const kahoy = new Tree()

  kahoy.insert(10)
  expect(kahoy.find(10)).toBeTruthy()
  expect(kahoy.find(4)).toBeFalsy()
})

test('check node placement', () => {
  const kahoy = new Tree()
  kahoy.insert(10)
  kahoy.insert(6)
  kahoy.insert(18)

  expect(kahoy.root.value).toBe(10)
  expect(kahoy.root.left.value).toBe(6)
  expect(kahoy.root.right.value).toBe(18)
})
